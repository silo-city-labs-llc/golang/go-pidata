package pihardware

import (
	"os/exec"
	"strings"
)

type Cpu struct {
	CPU      string
	Hardware string
	Revision string
	Serial   string
}

var CpuInfo Cpu

func GetHardwareInfo() {
	out, _ := exec.Command("/bin/cat", []string{"/proc/cpuinfo"}...).Output()
	outstring := strings.TrimSpace(string(out))
	lines := strings.Split(outstring, "\n")

	for _, line := range lines {
		fields := strings.Split(line, ":")
		if len(fields) < 2 {
			continue
		}
		key := strings.TrimSpace(fields[0])
		value := strings.TrimSpace(fields[1])

		switch key {
		case "model name":
			CpuInfo.CPU = value
		case "Hardware":
			CpuInfo.Hardware = value
		case "Revision":
			CpuInfo.Revision = value
		case "Serial":
			CpuInfo.Serial = value
		}
	}
}
