
install

go get gitlab.com/silo-city-labs-llc/golang/go-pidata/pihardware

use

import pihardware "gitlab.com/silo-city-labs-llc/golang/go-pidata/pihardware"

GetHardwareInfo()
------

Example:
``` go
    package main

    import (
        pihardware "gitlab.com/silo-city-labs-llc/golang/go-pidata/pihardware"
    )

    func main() {
        pihardware.GetHardwareInfo()

        pihardware.CpuInfo.CPU
    }

```